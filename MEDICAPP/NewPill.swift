//
//  NewPill.swift
//  MEDICAPP
//
//  Created by Gabriela Viñas on 9/29/19.
//  Copyright © 2019 Felipe Rivera. All rights reserved.
//

import Foundation
import UIKit

class NewPill: UIViewController {
    
    // New Pill View Cotroller elements connected.
    @IBOutlet weak var pillNameLbl: UILabel!
    @IBOutlet weak var pillNameTxt: UITextField!
    @IBOutlet weak var pillTimePick: UIDatePicker!
    @IBAction func addPill(_ sender: UIButton) {
    }
    
}
