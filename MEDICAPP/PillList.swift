//
//  PillList.swift
//  MEDICAPP
//
//  Created by Gabriela Viñas on 9/29/19.
//  Copyright © 2019 Felipe Rivera. All rights reserved.
//

import Foundation
import UIKit
import os.log
import UserNotifications


class PillList: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //connection from the TableView to the ViewController
    @IBOutlet weak var tableView: UITableView!
    
    //pill list
    var pillList = [Pill]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        //load data when the ViewController is loaded.
        if let savedPills = loadPills() {
            pillList += savedPills
        }
    }
    
    // Unwinding the segue from the newPill view controller
    @IBAction func unwindToMain(sender:UIStoryboardSegue){
        
        // we need to create reference to the newPill view controller
        guard let newPillVC = sender.source as? NewPill else {return}
        
        // obtain new pill data and add it to our pill list
        let new = Pill(name:newPillVC.pillNameTxt.text!, time:newPillVC.pillTimePick.date)
        pillList.append(new)
        
        // sort pill list.
        // objet 1 compare object 2.
        // sorted by time(ascending)
        pillList.sort {
            $0.time.compare($1.time) == ComparisonResult.orderedAscending
        }
        
        //schedule pill notification
        let delegate = UIApplication.shared.delegate as? AppDelegate
        delegate?.schedulePillNotification(from: new, at: new.time)
        
        // now we have new pill.
        // repaint data and save list
        tableView.reloadData()
        savePills()
        
    }
    
    // ==== tableView config ====
    
    // Number of sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // Number of rows per section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pillList.count
    }
    
    // Cell configuration
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "cell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PillCell  else {
            fatalError("The dequeued cell is not an instance of PillTableViewCell.")
        }
        
        // fetches the appropiate pill for the data source layout.
        let pill = pillList[indexPath.row]
        
        cell.pillNameLabel.text = pill.pillName
        
        // --- time format ----
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateStyle = .none
        
        let timeTxt = formatter.string(from:pill.time)
        cell.pillTimeLabel.text? = timeTxt
        //----------------------
        
        // cell color based on pill status
        // TODO: use an enum object to avoid this integer
        if (pill.status == 1){
            cell.backgroundColor = UIColor.green
            cell.pillImage.image = #imageLiteral(resourceName: "greenTick")
        }
        else
        {
            if (pill.status == 0)
            {
                cell.backgroundColor = UIColor.orange
            }
            else
            {
                cell.backgroundColor = UIColor.white
                cell.pillImage.image = #imageLiteral(resourceName: "pill")
            }
        }
        
        // making cell rows rounded
        cell.layer.cornerRadius = 12
        cell.layer.masksToBounds = true
        cell.layer.borderWidth = 1
        
        return cell
    }
    
    // Configuration for a cell element pressed.
    internal  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // we have the cell pressed in the indexPath var.
        // So, don't need a selected cell row.
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        
        // obtain the pill data
        let pill = pillList[indexPath.row]
        
        // manage the status
        // TODO: Use a better option. (switch)
        
        // si el status es 0 i prenem hem d'eliminar tb la notificatió extra.
        
        if (pill.status == -1){
            // Si tenim la pastilla com a "nova" i ens la prenem.
            // Podem eliminar la notificació actual i la podem tornar a crear.
            // Això farà que es torni a reproduir el proper dia.
            
            pill.status = 1 // new status = taken
            // remove request notification with uuid
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [pill.uuid])
            
            // asignem nova petició de notificació a la mateixa hora del pill.time.
            // aixo fara que torni a activar-se el proper dia.
            let delegate = UIApplication.shared.delegate as? AppDelegate
            delegate?.schedulePillNotification(from: pill, at: pill.time)
            
        }
        else{
            if (pill.status == 0){
                // Si tenim la pastilla com a later i ens la prenen.
                // posem-la a taken i eliminem el request notification pending
                
                pill.status = 1  // new status = taken
                // identifier per als pending recodem que es: uid + "l"
                let pendingIdentifier = pill.uuid + "l"
                print (pendingIdentifier)
                
                // remove pending request notification with uuid
                UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [pendingIdentifier])
                
                // remove also usual notification to be created for the next day.
                UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [pill.uuid])
                
                // asignem nova petició de notificació a la mateixa hora del pill.time.
                // aixo fara que torni a activar-se el proper dia.
                let delegate = UIApplication.shared.delegate as? AppDelegate
                delegate?.schedulePillNotification(from: pill, at: pill.time)
                
                // delete -1 al counter del badge
                UIApplication.shared.applicationIconBadgeNumber -= 1
            }
            else{
                pill.status = -1 // current status = new
            }
        }
        
        // Repaint the table as status may have changed.
        tableView.reloadData()
        
    }
    
    // Swipe configuration
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        //TODO: > Answer why this function just right swipe?
        //      > Make left swipe
        
        // fetches the appropiate pill for the data source layout.
        let pill = pillList[indexPath.row]
        
        // Create a later option.
        // This option will be used when a pill is postponed
        let laterAction = UIContextualAction(style: .destructive, title: "Posponer") { (action, view, handler) in
            os_log("Later pill action Tapped.", log: OSLog.default, type: .debug)
            
            // === TAKING LATER A PILL ===
            
            //schedule pill notification + 30 min
            let delegate = UIApplication.shared.delegate as? AppDelegate
            let laterTime = pill.time.addingTimeInterval(TimeInterval(30.0 * 60.0)) // we are adding 30 min from the initial time
            pill.uuid = pill.uuid + "l"
            delegate?.schedulePillNotification(from: pill, at:laterTime)
            
            // add 1 al counter del badge
            UIApplication.shared.applicationIconBadgeNumber += 1
            pill.status = 0
            
            tableView.reloadData()
        }
        laterAction.backgroundColor = UIColor.orange
        
        // Create a delete option.
        // This option will be used when a pill is deleted
        let deleteAction = UIContextualAction(style: .destructive, title: "Eliminar") { (action, view, handler) in
            os_log("Delete pill action Tapped.", log: OSLog.default, type: .debug)
            
            // === DELETING A PILL ===
            
            //schedule pill notification
            //let delegate = UIApplication.shared.delegate as? AppDelegate
            //delegate?.schedulePillNotification(from: new, at: new.time)
            
            // remove request notification with uuid
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [pill.uuid])
            
            // Delete the row from the data source and save pills
            self.pillList.remove(at: indexPath.row)
            tableView.reloadData()
            self.savePills()
            
        }
        deleteAction.backgroundColor = .red
        
        let configuration = UISwipeActionsConfiguration(actions: [laterAction,deleteAction])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
    
    // ==== END tableView config ====
    
    // Save pills
    private func savePills() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(pillList, toFile: Pill.ArchiveURL.path)
        if isSuccessfulSave {
            os_log("Pills successfully saved.", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save pills...", log: OSLog.default, type: .error)
        }
    }
    
    // Load pills
    private func loadPills() -> [Pill]?  {
        return NSKeyedUnarchiver.unarchiveObject(withFile: Pill.ArchiveURL.path) as? [Pill]
    }
}

