//
//  PillCell.swift
//  MEDICAPP
//
//  Created by Gabriela Viñas on 9/29/19.
//  Copyright © 2019 Felipe Rivera. All rights reserved.
//

import Foundation
import UIKit

// Pill cell configuration
class PillCell: UITableViewCell {
    
    // elements of the cell connected.
    @IBOutlet weak var pillNameLabel: UILabel!
    @IBOutlet weak var pillTimeLabel: UILabel!
    @IBOutlet weak var pillImage: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
